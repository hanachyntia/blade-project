@extends('layout.main')

@section('title', 'Daftar Film')

@section('container')
<div class="container">
    <div class="row">
        <div class="col-10">
            <h1 class="mt-3">Daftar Film</h1>

            <!-- <a href="/mahasiswas/create"></a> -->

            <table class="table">
                <thead class="table table-dark table-striped  ">
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Judul</th>
                        <th scope="col">Rilis</th>
                        <th scope="col">Rating</th>
                        <th scope="col">Kategori</th>
                        <!-- <th scope="col">Aksi</th> -->
                    </tr>
                </thead>
                <tbody>
                @foreach( $film as $flm)
                <tr>
                <th scope="row">{{ $loop->iteration }}</th>
                <td>{{ $flm->judul }} </td>
                <td>{{ $flm->rilis }}</td>
                <td>{{ $flm->rating }}</td>
                <td>{{ $flm->kategori }}</td>
                <!-- <td>
                    <a href="" class="btn btn-success">edit</a>
                    <a href="" class="btn btn-danger">delete</a>
                    <!-- <a href="#" class="card-link">kembali</a> -->
                <!-- </td> -->
                </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection